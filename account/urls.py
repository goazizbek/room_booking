from django.urls import path

from .views import LoginView, UserListView, ChangePasswordView, MyProfileView

urlpatterns = [
    path('users/', UserListView.as_view()),
    path('login/', LoginView.as_view()),
    path('my/change/password/', ChangePasswordView.as_view()),
    path('my/profile/', MyProfileView.as_view()),
]
