try:
    from threading import local, current_thread
except ImportError:
    from django.utils._threading_local import local

_thread_locals = local()


def get_device_type(request):
    # print('im here!')
    result = request.META.get('HTTP_DEVICE_TYPE') or \
             request.META.get('DEVICE_TYPE') or \
             request.COOKIES.get("HTTP_DEVICE_TYPE") or \
             request.COOKIES.get("DEVICE_TYPE")
    # print("\n\n\n", result ,"\n\n\n")
    return result


class GlobalUserMiddleware(object):
    """
    Sets the current authenticated user in threading locals

    Usage example:
        from app_name.middleware import get_current_user
        user = get_current_user()
    """

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        setattr(_thread_locals, 'user_{0}'.format(current_thread().name), request.user)
        setattr(_thread_locals, 'device_{0}'.format(current_thread().name), get_device_type(request))

        response = self.get_response(request)
        key = 'user_{0}'.format(current_thread().name)

        if hasattr(_thread_locals, key):
            delattr(_thread_locals, key)
        return response
