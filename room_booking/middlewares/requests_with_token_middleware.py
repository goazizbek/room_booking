from rest_framework import exceptions, status
from django.contrib.auth import get_user_model
from rest_framework_jwt.authentication import JSONWebTokenAuthentication, jwt_get_username_from_payload
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AnonymousUser
from django.http import JsonResponse
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.authentication import SessionAuthentication


class UserJSONWebTokenAuthentication(JSONWebTokenAuthentication):
    def authenticate_credentials(self, payload):
        """
        Returns an active user that matches the payload's user id and email.
        """
        User = get_user_model()
        username = jwt_get_username_from_payload(payload)

        if not username:
            msg = _('Invalid payload.')
            raise exceptions.AuthenticationFailed(msg)

        try:
            user = User.objects.get_by_natural_key(username)
        except User.DoesNotExist:
            msg = _('Invalid signature.')
            raise exceptions.AuthenticationFailed(msg)

        if not user.is_active:
            msg = _('User account is disabled.')
            raise exceptions.AuthenticationFailed(msg)
        # if not user.is_registered:
        #     msg = _('User not confirmed the registration')
        #     raise exceptions.AuthenticationFailed(msg, code='not_registered')
        iat = payload.get('orig_iat', 0)
        if not user or (user.password_update_time and user.password_update_time and iat < user.password_update_time):
            msg = _('Invalid token. Password has been changed.')
            raise exceptions.AuthenticationFailed(msg, code='401')
        return user


class JWTAuthenticationMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        try:
            user_jwt = UserJSONWebTokenAuthentication().authenticate(request)
            if user_jwt is not None:
                # store the first part from the tuple (user, obj)
                user = user_jwt[0]
                if not isinstance(user, AnonymousUser):
                    request.user = user
        except AuthenticationFailed as error:
            return JsonResponse({"message": error.detail}, status=status.HTTP_401_UNAUTHORIZED)
        return self.get_response(request)


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening
