from django.utils import timezone
from django.db.models import Q

from book.models import ConferenceModel


def update_status():
    print('job worked')
    ConferenceModel.objects.filter(end_date__lt=timezone.now()).exclude(
        Q(status='Yakunlandi') | Q(status='Bekor qilindi')).update(status='Yakunlandi')
