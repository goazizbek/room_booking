import django_filters
from django_filters.rest_framework import FilterSet, ChoiceFilter
from .models import ConferenceModel


class ConferenceFilter(FilterSet):
    # ids = django_filters.MultipleChoiceFilter(field_name='id')
    status = ChoiceFilter(choices=ConferenceModel.STATUS, empty_label='Any')
    meeting_type = ChoiceFilter(choices=ConferenceModel.TYPE, empty_label='Any')
    meeting_room = ChoiceFilter(choices=ConferenceModel.HALLS, empty_label='Any')

    class Meta:
        model = ConferenceModel
        fields = ('status', 'meeting_type', 'meeting_room')
