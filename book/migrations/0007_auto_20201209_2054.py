# Generated by Django 3.1.3 on 2020-12-09 15:54

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('book', '0006_auto_20201209_1609'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='filialmodel',
            name='conference',
        ),
        migrations.RemoveField(
            model_name='thememodel',
            name='conference',
        ),
        migrations.AddField(
            model_name='conferencemodel',
            name='filial',
            field=models.ManyToManyField(to='book.FilialModel'),
        ),
        migrations.AddField(
            model_name='conferencemodel',
            name='participant',
            field=models.ManyToManyField(related_name='meeting_participants', to=settings.AUTH_USER_MODEL),
        ),
        migrations.RemoveField(
            model_name='conferencemodel',
            name='department',
        ),
        migrations.AddField(
            model_name='conferencemodel',
            name='department',
            field=models.ManyToManyField(to='book.DepartmentModel'),
        ),
    ]
