from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from .serializers import ConferenceModelSerializer, ThemeSerializer, FilialSerializer, DepartmentSerializer, \
    ConferenceListSerializer
from .models import ConferenceModel, ThemeModel, FilialModel, DepartmentModel
from .filters import ConferenceFilter


class ConferenceVieSet(viewsets.ModelViewSet):
    queryset = ConferenceModel.objects.all()
    filter_class = ConferenceFilter

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'get' or self.action == 'retrieve':
            return ConferenceListSerializer
        return ConferenceModelSerializer

    def get_queryset(self):
        user = self.request.user
        qs = super(ConferenceVieSet, self).get_queryset()
        if self.request.GET.get('ids'):
            qs = qs.filter(id__in=self.request.GET.get('ids').split(','))
        if user.is_superuser:
            return qs.order_by('-created_at')
        qs = qs.filter(user=user)
        return qs.order_by('-created_at')

    @action(methods=['put'], detail=True)
    def cancel(self, request, pk=None):
        instance = self.get_object()
        if not instance:
            return Response({"status": "FAIL", "detail": "Konferensiya topilmadi"})
        instance.status = "Bekor qilindi"
        instance.save()

        return Response({"status": "OK", "detail": "Konferensiya bekor qilindi"})


class ThemeVieSet(viewsets.ModelViewSet):
    queryset = ThemeModel.objects.all()
    serializer_class = ThemeSerializer


class DepartmentVieSet(viewsets.ModelViewSet):
    queryset = DepartmentModel.objects.all()
    serializer_class = DepartmentSerializer


class FilialVieSet(viewsets.ModelViewSet):
    queryset = FilialModel.objects.all()
    serializer_class = FilialSerializer
