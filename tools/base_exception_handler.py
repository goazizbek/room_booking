import json
from django.http import JsonResponse
from rest_framework.exceptions import ValidationError
from rest_framework.views import exception_handler

from tools.exceptions import ValidationError2


def base_exception_handler(exc, context):
    response = exception_handler(exc, context)
    # print(json.dumps(context['request'].data))
    status_code = 400
    code = "validation_error"

    # check that a ValidationError exception is raised
    message = ""
    if isinstance(exc, ValidationError):
        # here prepare the 'custom_error_response' and
        # set the custom response data on response object

        for key, value in response.data.items():
            message += "%s%s\n" % (
                key + ": " if key else "", "".join(value) if value and isinstance(value, (tuple, list)) else value)

    # check that a ValidationError exception is raised
    if isinstance(exc, ValidationError2):
        # here prepare the 'custom_error_response' and
        # set the custom response data on response object

        if isinstance(response.data, dict):
            for key, value in response.data.items():
                message += "%s%s\n" % (
                    key + ": " if key else "", "".join(value) if value and isinstance(value, (tuple, list)) else value)

        if isinstance(response.data, (list, tuple)):
            for value in response.data:
                message += "%s\n" % ("".join(value) if value and isinstance(value, (tuple, list)) else value)
    if response is not None:
        status_code = response.status_code
    else:
        if exc and exc.args:
            for value in exc.args:
                message += "%s\n" % ("".join(value) if value and isinstance(value, (tuple, list)) else value)
                status_code = 500
                code = "attribute_error"

    return JsonResponse(data={"message": message, "code": code}, status=status_code)
