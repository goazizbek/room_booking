from django.contrib import admin

from .models import DepartmentModel, ConferenceModel, ThemeModel, FilialModel
from account.models import User


class ThemeInline(admin.TabularInline):
    model = ThemeModel


class DepartmentInline(admin.TabularInline):
    model = DepartmentModel


class FilialInline(admin.TabularInline):
    model = FilialModel


# class UserInline(admin.TabularInline):
#     fields = ['username', 'first_name', 'last_name']
#     model = User


# class ConferenceAdmin(admin.ModelAdmin):
#     inlines = [DepartmentInline, FilialInline, ThemeInline]


admin.site.register(ConferenceModel)
admin.site.register(ThemeModel)
admin.site.register(DepartmentModel)
admin.site.register(FilialModel)
