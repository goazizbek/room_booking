import django

django.setup()
from account.models import User
from book.models import DepartmentModel, FilialModel
import time
import csv

print('start')
with open('users.csv', newline='') as f:
    reader = csv.reader(f, delimiter=',')
    user_count = 0
    for row in reader:
        User.objects.create(username=row[0], first_name=row[1], last_name=row[2])
        user_count += 1
    print(user_count)

time.sleep(15)
print('start')
users = User.objects.filter()
print(len(users))
count = 0
for user in users:
    user.set_password('test2020')
    user.save()
    print(user)
    count += 1

print(count)
print('end')
print('start department')
with open('departments.json') as f:
    import json

    deps = json.load(f)
for d in deps:
    dp = DepartmentModel(name=d['name'])
    dp.save()

time.sleep(5)
print('start flial')
with open('filial.csv', newline='') as f:
    reader = csv.reader(f, delimiter=',')
    user_count = 0
    for row in reader:
        FilialModel.objects.create(name=row[0])
        user_count += 1
    print(user_count)