# Generated by Django 3.1.3 on 2020-12-09 11:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('book', '0005_auto_20201209_1435'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='departmentmodel',
            name='conference',
        ),
        migrations.AddField(
            model_name='conferencemodel',
            name='department',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='book.departmentmodel'),
        ),
    ]
