from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from calendar import timegm
from datetime import datetime
from book.models import ConferenceModel


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, password, **extra_fields):
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, password, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        if password is None:
            raise TypeError(
                'Superuser must have a password'
            )
        user = self.create_user(username=username, password=password, **extra_fields)

        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)

        return user


class User(AbstractUser):
    username = models.CharField(blank=True, null=True, max_length=50, unique=True)
    first_name = models.CharField(max_length=255, null=True)
    last_name = models.CharField(max_length=250, null=True)
    password_update_time = models.IntegerField(default=0, null=True)
    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'username'
    objects = UserManager()

    def __str__(self):
        return self.username

    def dict(self):
        return {
            'id': self.id,
            'email': self.username,
            'first_name': self.last_name,
            'last_name': self.last_name
        }

    def set_password(self, raw_password):
        super(User, self).set_password(raw_password=raw_password)
        self.password_update_time = timegm(datetime.utcnow().utctimetuple())
