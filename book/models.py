from django.db import models
from django.conf import settings


# Create your models here.

class TimeStampModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class DepartmentModel(models.Model):
    index = models.CharField(max_length=50, blank=True, null=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class FilialModel(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class ThemeModel(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return '{}'.format(self.name)


class ConferenceModel(TimeStampModel):
    BIG_HALL = 'Katta Zal'
    SMALL_HALL = 'Kichik Zal'
    THIRD_HALL = 'Akvarium'
    HALLS = (
        (BIG_HALL, ('Katta Zal')),
        (SMALL_HALL, ('Kichik Zal')),
        (THIRD_HALL, ('Akvarium'))
    )

    ZOOM = 'Zoom'
    CISCO = 'Cisco'
    TYPE = (
        (ZOOM, ('Zoom')),
        (CISCO, ('Cisco'))
    )

    STARTED = 'Boshlandi'
    FINISHED = 'Yakunlandi'
    CANCELED = 'Bekor qilindi'
    NEW = 'Yangi'

    STATUS = (
        (STARTED, ('Boshlandi')),
        (FINISHED, ('Yakunlandi')),
        (CANCELED, ('Bekor qilindi')),
        (NEW, ('Yangi')),
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
    department = models.ManyToManyField(DepartmentModel)
    filial = models.ManyToManyField(FilialModel, blank=True)
    participant = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='meeting_participants')
    theme = models.ManyToManyField(ThemeModel)
    lead_by = models.CharField(max_length=100)
    description = models.CharField(max_length=255, null=True)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    meeting_type = models.CharField(max_length=50, choices=TYPE)
    meeting_room = models.CharField(max_length=50, choices=HALLS)
    status = models.CharField(choices=STATUS, default=NEW, max_length=50)

    def __str__(self):
        return self.lead_by