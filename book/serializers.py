from rest_framework import serializers
from django.db.models import Q
from django.utils import timezone
from .models import ConferenceModel, DepartmentModel, ThemeModel, FilialModel
from account.serializers import UserSerializer


class FilialSerializer(serializers.ModelSerializer):
    class Meta:
        model = FilialModel
        fields = '__all__'


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = DepartmentModel
        fields = '__all__'


class ThemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ThemeModel
        fields = '__all__'


class ConferenceModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConferenceModel
        fields = '__all__'

    def validate(self, attrs):
        if 'start_date' in attrs:
            qs = ConferenceModel.objects.filter(start_date=attrs.get('start_date'),
                                                meeting_room=attrs.get('meeting_room')).exclude(
                Q(status='Yakunlandi') | Q(status='Bekor qilindi'))
            if qs.count():
                raise serializers.ValidationError(detail='Xona band! Boshqa xona tanlang!', code='bad_request')

            qs = ConferenceModel.objects.filter(
                start_date__lte=attrs.get('end_date'), end_date__gte=attrs.get('start_date'),
                meeting_room=attrs.get('meeting_room')).exclude(
                Q(status='Yakunlandi') | Q(status='Bekor qilindi'))
            if qs.count():
                raise serializers.ValidationError(detail="Xonada majlis o'tkazilyapti", code='bad_request')

        return attrs

    def update(self, instance, validated_data):
        instance = super(ConferenceModelSerializer, self).update(instance, validated_data)
        qs = ConferenceModel.objects.filter(Q(id=instance.id),
                                            Q(end_date__lte=timezone.now()) | Q(status='Bekor qilindi'))
        if qs.count():
            raise serializers.ValidationError(detail="Majlis yakunlangan yoki bekor qilingan", code="bad_request")
        return instance


class ConferenceListSerializer(serializers.ModelSerializer):
    department = DepartmentSerializer(many=True, read_only=True)
    theme = ThemeSerializer(many=True, read_only=True)
    participant = UserSerializer(many=True, read_only=True)
    filial = FilialSerializer(many=True, read_only=True)

    class Meta:
        model = ConferenceModel
        fields = '__all__'
