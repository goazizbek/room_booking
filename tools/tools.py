from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Model
from rest_framework import serializers
from django.apps import apps


class IDField:
    def to_internal_value(self, data):
        if data and isinstance(data, dict):
            return data.get('id', data)
        return data


class SelectItemField(serializers.PrimaryKeyRelatedField):
    def __init__(self, model, extra_field="name", **kwargs):
        kwargs["pk_field"] = IDField()
        if not isinstance(model, str):
            self.Model = model
        else:
            self.Model = apps.get_model(model.split('.')[0], model.split('.')[1])
        kwargs['queryset'] = self.Model.objects.all() if not kwargs.get('read_only', False) else None
        kwargs['allow_null'] = not kwargs.get('required')
        self.extra_field = extra_field if isinstance(extra_field, (list, tuple)) else [extra_field]
        super().__init__(**kwargs)

    def to_internal_value(self, data):
        if self.pk_field is not None:
            data = self.pk_field.to_internal_value(data)
        if data:
            try:
                return self.get_queryset().get(pk=data)
            except ObjectDoesNotExist:
                self.fail('does_not_exist', pk_value=data)
            except (TypeError, ValueError):
                self.fail('incorrect_type', data_type=type(data).__name__)

    def to_representation(self, value):
        item = self.Model.objects.get(pk=value.pk)
        result = {'id': value.pk}
        for extra_field in self.extra_field:
            if extra_field and item:
                v = getattr(item, extra_field, None)
                if isinstance(v, Model):
                    if hasattr(v, 'as_select_item'):
                        v = v.as_select_item()
                    else:
                        v = {"id": v.pk, "name": str(v)}
                result[extra_field] = v
        return result
