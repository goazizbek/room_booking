from django.urls import path, include
from rest_framework import routers
from .views import *


router = routers.DefaultRouter()

router.register(r'conference', ConferenceVieSet)
router.register(r'department', DepartmentVieSet)
router.register(r'filial', FilialVieSet)
router.register(r'theme', ThemeVieSet)

urlpatterns = [
    path('', include(router.urls))
]