from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _
from .models import User


class UserAdmin(DjangoUserAdmin, admin.ModelAdmin):
    fieldsets = (
        (_('Main'), {'fields': ('username', 'password',)}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login',)}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2', 'first_name', 'last_name',),
        }),
    )
    list_display = ('get_fullname', 'username',)
    search_fields = ('username', 'first_name', 'last_name',)

    @staticmethod
    def get_fullname(obj):
        return "{} {}".format(obj.first_name, obj.last_name)


admin.site.register(User, UserAdmin)
